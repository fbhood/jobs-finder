#!/usr/bin/env python3

import requests as r
import os
from dotenv import load_dotenv
load_dotenv()

# API ref: https://www.reed.co.uk/developers/Jobseeker


def get_jobs_from_reed_uk(auth, url, query):
    """ ## GET Request REED.CO.UK jobs search
    Returns a list of dictionaries from the reed.co.uk api job search query
    :param auth = string containing the authentication api code
    :param url = string containing the api request url
    :param query = string containing the keywords to pass to the query
    """

    headers = {
        'Authorization': 'Basic {}'.format(auth),
        'Cookie': '__cfduid=decd69d40eec4686858eb9b3781c1c69d1589561506; jsSplitTestGroup=1; .ASPXANONYMOUS=_HIZ0ZyhXGnIJEDilCNHaQPj_m0UqImTF6CYxphl4sg782HaG6th6xYV6st7ZJwJSgWi7KEwMREARXEtxtmYyvLIQOjjP2Z7-zayzITJklq-EdvMIDDGH_tr-nE_7JIz88S6DA2'
    }
    params = {"keywords": query}
    response = r.request("GET", url, params=params,
                         headers=headers)

    results = response.json()
    return results


def get_jobs_list(auth, url, query):
    """ Returns a list of Dictionaries with jobs details """
    results = get_jobs_from_reed_uk(auth, url, query)
    jobs_list = results['results']
    return jobs_list


def save_reed_jobs(auth, search_url, query, exclude, file_name):
    """ Save jobs from the REST API into a CSV file.  """
    import csv
    import datetime
    jobs_list = get_jobs_list(auth, search_url, query)
    count = 0
    filtered_list = []
    for job_dict in jobs_list:
        if job_dict["minimumSalary"] != None and job_dict["minimumSalary"] > 30000.00:
            job_id = job_dict["jobId"]
            company = job_dict["employerName"]
            title = job_dict["jobTitle"].lower()
            desc = job_dict["jobDescription"].lower()
            salary = [job_dict["minimumSalary"], job_dict["maximumSalary"]]
            url = job_dict["jobUrl"]
            posted_on = job_dict["date"]
            closing_date = job_dict["expirationDate"]

            if any(x in title for x in exclude) or any(x in desc for x in exclude):
                continue
            else:
                filtered_list.append(
                    {"Fetch Date": datetime.datetime.now(), "Source": "Reed.co.uk", "ID": job_id, "Company": company, "Title": title, "Description": desc, "Salary": salary, "Url": url, "Posted On": posted_on, "Closing Date": closing_date})
                count += 1

    with open(file_name, "w+") as f:
        filednames = ["Fetch Date", "Source", "ID", "Company", "Title", "Description", "Salary",
                      "Url", "Posted On", "Closing Date"]
        writer = csv.DictWriter(f, fieldnames=filednames)
        writer.writeheader()
        writer.writerows(filtered_list)

    print("Total Jobs from Reed.co.uk: {}".format(count))
