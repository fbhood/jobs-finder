#!/usr/bin/env python3
import datetime
import requests as r
from bs4 import BeautifulSoup


def get_content(url, cat, skills, exclude):
    """ Fetch the Remotive.io jobs API and returns a list of selected jobs """
    params = {"category": cat}
    request = r.get(url, params=params)
    jobs_list = request.json()["jobs"]
    sorted_jobs = []
    count = 0
    for job in jobs_list:
        job_id = job["id"]
        company = job["company_name"]
        title = job["title"]
        url = job["url"]
        tags = job["tags"]
        description = job["description"]
        location = job['candidate_required_location']
        desc = BeautifulSoup(description, "html.parser")
        salary = job["salary"]
        posted_on = job["publication_date"]
        if any(x in tags for x in skills):
            if location == "Anywhere" or location == "Europe Only":
                if any(x in tags for x in exclude):
                    print("!!! Alert there are unwanted skills here")
                sorted_jobs.append(
                    {"Fetch Date": datetime.datetime.now(), "Source": "Remotive.io", "ID": job_id, "Company": company, "Title": title, "Description": desc.text, "Salary": salary, "Url": url, "Posted On": posted_on, "Closing Date": "", "Skills": tags})
                count += 1
    print("Total Jobs from Remotive.io: {}".format(count))
    return sorted_jobs


def save_remotive_jobs(url, cat, skills, exclude, file_name):
    """ Stores all jobs into a .csv file """
    import csv
    jobs = get_content(url, cat, skills, exclude)
    fieldnames = ["Fetch Date", "Source", "ID", "Company", "Title", "Description", "Salary", "Url",
                  "Posted On", "Closing Date", "Skills"]
    with open(file_name, "a") as f:
        writer = csv.DictWriter(f, fieldnames=fieldnames)
        writer.writeheader()
        writer.writerows(jobs)
