#!/usr/bin/env python3
import csv
from track_applications import check_application_id_exists


def add_html_head():
    """ Returns the start html page markup """
    markup_head = """
        <!doctype html>
        <html lang="en">
            <head>
                <title>Jobs</title>
                <!-- Required meta tags -->
                <meta charset="utf-8">
                <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

                <!-- Bootstrap CSS -->
                <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
            </head>
            <body>
                <div class="jumbotron jumbotron-fluid">
                    <div class="container">
                        <h1 class="display-3">Jobs</h1>
                        
                    </div>
                </div>
                <div class="container pt-5">
                    <ul class="list-group">
        """
    return markup_head.strip("\n")


def add_markup_footer():
    """ Returns the end html page markup """
    markup_footer = """ 
                </ul>
            </div>

        </body>
    </html>
    """
    return markup_footer


def create_html_list(file, source_file):
    with open(file, "w+") as file:
        file.writelines(add_html_head())
        with open(source_file) as f:
            for line in csv.DictReader(f):
                title = line["Title"]
                source = line["Source"]
                url = line["Url"]
                desc = line["Description"]
                salary = line["Salary"]
                job_id = line["ID"]
                company = line["Company"]
                posted_on = line["Posted On"]
                closing_date = line["Closing Date"]
                #skills = line["Skills"]

                if job_id == check_application_id_exists(job_id):
                    print(True, job_id, check_application_id_exists(job_id))
                    bg_color = "bg-danger"
                    btn_status = "warning"
                    status = "Applied"
                else:
                    print(False, job_id, check_application_id_exists(job_id))
                    bg_color = "bg-white"
                    btn_status = "success"
                    status = "Not yet Applied"

                markup = f"""
                <li class="list-group-item {bg_color}">
                    <h4>{title} | <em>ID:[{job_id}]</em></h4>
                    <p><em>Company: {company}</em></p>
                    <p>Salary: {salary}</p>
                    <p>{desc}</p>
                    <p><a class="btn btn-sm btn-primary" href="{url}" target="_blank">View</a></p>
                    <p><a class="btn btn-sm btn-{btn_status}" href="#" data-jobId="{job_id}" data-title="{title}" data-company="{company}" data-url="{url}" data-job-source="{source}">{status}</a></p>
                    <p><em>Source: {source}</em></p>
                </li>""".strip("\n")
                # .format(title, job_id, company, salary, desc, url, source)
                file.writelines(markup)
        file.writelines(add_markup_footer())
