#!/usr/bin/env python3
import sqlite3


def connect_db(db_name="data/jobs.db"):
    """ Creates a db connection """
    connect = sqlite3.connect(db_name)
    cursor = connect.cursor()
    return cursor, connect


def close_connection(connection):
    connection.commit()
    connection.close()


def create_table():
    curs, connect = connect_db()
    curs.execute(
        """SELECT count(name) FROM sqlite_master WHERE type='table' AND name='applications';""")
    if curs.fetchone()[0] == 1:
        print("Table Exists")
        return
    else:
        print('Table does not exist. Creating table!')
        curs.execute(
            "CREATE TABLE applications (job_id integer, company text, title text, source text, description text)")
    close_connection(connect)


def save_application_details(job_id, company, title, source, description):
    curs, connect = connect_db()
    curs.execute(""" INSERT INTO applications VALUES (:job_id, :company, :title, :source, :description) """,
                 {
                     "job_id": job_id,
                     "company": company,
                     "title": title,
                     "source": source,
                     "description": description
                 }
                 )
    print("Insert Records Complete")
    close_connection(connect)


def see_all_applications():
    curs, connect = connect_db()
    try:
        curs.execute(""" SELECT *, oid FROM applications """)
        records = curs.fetchall()
        print(records)
    except Exception as e:
        print(e)
    close_connection(connect)


see_all_applications()


def check_application_id_exists(id):
    curs, connect = connect_db()

    curs.execute(""" SELECT job_id FROM applications WHERE job_id=:job_id""",
                 {"job_id": id}
                 )
    result = curs.fetchone()
    close_connection(connect)
    return str(result).strip("(),")


# Create db table if it does not exists.
create_table()
