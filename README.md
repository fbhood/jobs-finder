# Jobs Finder

Returns a filtered list of jobs from the Reed.co.uk and Remotive.io APIs.

## Setup

First clone the repository locally
`git clone https://fbhood@bitbucket.org/fbhood/jobs-finder.git`

### Then create a .env file:

cd into the repository and create a .env file in the main project folder:
`cd jobs-finder`
`touch .env`

Add the following lines to the .env file and replace the reed_auth_here and reed_key_here with your reed.co.uk api credentials.

`REED_AUTH=reed_auth_here`
`REED_API=reed_key_here`

You can get api credentials from reed [here](https://www.reed.co.uk/developers/Jobseeker).

## Run the script:

The script accepts two parameters:

- the first is a serch term for Reed.co.uk (can be any job title)
- the second is the category for the remotive.io website (one of: "Software Development", "Customer Support / Customer Success", "Design", "Marketing / Sales", "Product", "All others")

`./app.py "Front end developer" "Software Development"`

If no parameters are passed it runs the default settings:

- reed_query = "Full-stack developer"
- remotive_query = "Software Development"

## Visit the jobs list page:

The results are stored in a csv file located inside the data/ folder, parsed and converted to an html file with a list of all jobs located
at `view/index.html`.
You can rightclick on it and open it in the browser to see the results.

## Roadmap:

- Add a way to mark a job as applied and store data in the db
