#!/usr/bin/env python3
from scrap_reed import save_reed_jobs
from scrap_remotive import save_remotive_jobs
from convert_html import create_html_list
import os
import sys
from dotenv import load_dotenv
load_dotenv()

if len(sys.argv) == 3:
    reed_query = sys.argv[1]
    remotive_query = sys.argv[2]
elif len(sys.argv) == 2:
    reed_query = sys.argv[1]
else:
    reed_query = "Backend developer"
    remotive_query = "Software Development"

# Get Reed.co.uk Jobs
search_url = "https://www.reed.co.uk/api/1.0/search"
query = reed_query
reed_auth = os.getenv("REED_AUTH")
exclude = ["react", "java", "c#", ".net", "c++"]
save_reed_jobs(reed_auth, search_url, query, exclude, "data/jobs.csv")

# Get Remotive.io Jobs
website_url = "https://remotive.io/api/remote-jobs"
query = remotive_query
skills = ["laravel", "php", "python"]
exclude = ["react", "react js", "C#", "AWS"]
save_remotive_jobs(website_url, query, skills,
                   exclude, "data/jobs.csv")


# Create the html jobs view page
create_html_list("view/index.html", "data/jobs.csv")
